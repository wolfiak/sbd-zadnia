//zad2
SET SERVEROUTPUT ON;
SHOW ERRORS;
DECLARE
	CURSOR lol IS SELECT pracownicy.nazwisko,zatrudnienie.placa_pod,zatrudnienie.premia FROM pracownicy,zatrudnienie WHERE pracownicy.id_prac=zatrudnienie.id_prac;
	stosunek NUMBER;
	tmp lol%ROWTYPE;
BEGIN
	FOR tmp IN lol LOOP
		stosunek:=0;
		
		stosunek:=tmp.placa_pod/NVL(tmp.premia,0);
		DBMS_OUTPUT.PUT_LINE(tmp.nazwisko||' '||stosunek);
	END LOOP;
	
	
EXCEPTION 
WHEN ZERO_DIVIDE THEN
	stosunek:=99;
	DBMS_OUTPUT.PUT_LINE('Nie dziel przez zero');
END;

//ZAD1

DECLARE
	CURSOR lol(p NUMBER) IS SELECT pracownicy.nazwisko,zatrudnienie.placa_pod,zatrudnienie.premia FROM pracownicy,zatrudnienie WHERE pracownicy.id_prac=zatrudnienie.id_prac AND pracownicy.id_prac=p;
	stosunek NUMBER;
	tmp lol%ROWTYPE;
BEGIN
		FOR tmp IN lol(1) LOOP
		stosunek:=0;
		
		stosunek:=tmp.placa_pod/NVL(tmp.premia,0);
		DBMS_OUTPUT.PUT_LINE(tmp.nazwisko||' '||stosunek);
		END LOOP;
	
	
EXCEPTION 
WHEN ZERO_DIVIDE THEN
	stosunek:=99;
	DBMS_OUTPUT.PUT_LINE('Nie dziel przez zero');
END;


//ZAD3

show errors;
set serveroutput on;
CREATE OR REPLACE PROCEDURE info(id NUMBER)
IS
	CURSOR lol(p NUMBER) IS  SELECT pracownicy.imie,pracownicy.nazwisko,zatrudnienie.placa_pod,jednostki.nazwa_jedn FROM pracownicy,jednostki,zatrudnienie WHERE pracownicy.id_prac=zatrudnienie.id_prac AND zatrudnienie.id_jedn=jednostki.id_jedn AND pracownicy.id_prac=p;
	tmp lol%ROWTYPE;
BEGIN


	FOR tmp IN lol(id) LOOP
		DBMS_OUTPUT.PUT_LINE('IMIE: '||tmp.imie||' nazwisko: '||tmp.nazwisko||' placa pod: '||tmp.placa_pod||' nazwa jednostki: '||tmp.nazwa_jedn);
	END LOOP;
	
	EXCEPTION
	WHEN INVALID_CURSOR THEN
	DBMS_OUTPUT.PUT_LINE('ZLY KURSOR');
	WHEN INVALID_NUMBER THEN
	DBMS_OUTPUT.PUT_LINE('zla konwersja do liczby');
	WHEN TOO_MANY_ROWS THEN
	DBMS_OUTPUT.PUT_LINE('za duzo krotek');
	WHEN VALUE_ERROR THEN
	DBMS_OUTPUT.PUT_LINE('BLAD KONWERSJI DANYCH');
	WHEN OTHERS THEN
	DBMS_OUTPUT.PUT_LINE('INNY BLAD');
END;


BEGIN
	info(1);
END;

//ZAD4

show errors;
set serveroutput on;
CREATE OR REPLACE PROCEDURE info(id NUMBER)
IS
	CURSOR lol(p NUMBER) IS  SELECT pracownicy.imie,pracownicy.nazwisko,zatrudnienie.placa_pod,jednostki.nazwa_jedn,zatrudnienie.premia FROM pracownicy,jednostki,zatrudnienie WHERE pracownicy.id_prac=zatrudnienie.id_prac AND zatrudnienie.id_jedn=jednostki.id_jedn AND pracownicy.id_prac=p;
	tmp lol%ROWTYPE;
BEGIN


	FOR tmp IN lol(id) LOOP
		IF (tmp.premia=null) THEN
			Raise_application_error(-20101,'Nie ma premii');
		END IF;
		DBMS_OUTPUT.PUT_LINE('IMIE: '||tmp.imie||' nazwisko: '||tmp.nazwisko||' placa pod: '||tmp.placa_pod||' nazwa jednostki: '||tmp.nazwa_jedn||' premia: '||tmp.premia);
	END LOOP;
	
	EXCEPTION
	WHEN INVALID_CURSOR THEN
	DBMS_OUTPUT.PUT_LINE('ZLY KURSOR');
	WHEN INVALID_NUMBER THEN
	DBMS_OUTPUT.PUT_LINE('zla konwersja do liczby');
	WHEN TOO_MANY_ROWS THEN
	DBMS_OUTPUT.PUT_LINE('za duzo krotek');
	WHEN VALUE_ERROR THEN
	DBMS_OUTPUT.PUT_LINE('BLAD KONWERSJI DANYCH');
	WHEN NO_DATA_FOUND THEN
	DBMS_OUTPUT.PUT_LINE('Nie ma danych');
	WHEN OTHERS THEN
	DBMS_OUTPUT.PUT_LINE('INNY BLAD');
END;


BEGIN
	info(1);
END;


//zad5i6

show errors;
set serveroutput on;
CREATE OR REPLACE PROCEDURE lista(id IN NUMBER)
IS
	CURSOR lol(p NUMBER) IS SELECT pracownicy.nazwisko,jednostki.id_jedn FROM pracownicy,zatrudnienie,jednostki WHERE pracownicy.id_prac=zatrudnienie.id_prac AND jednostki.id_jedn=zatrudnienie.id_jedn AND jednostki.id_jedn=p;
	tmp lol%ROWTYPE;
	wyjatek EXCEPTION;
	PRAGMA EXCEPTION_INIT(wyjatek,100);
	licznik NUMBER;
	l NUMBER:=0;

BEGIN
	SELECT count(*) INTO licznik FROM jednostki WHERE id_jedn=id_jedn;
	IF(id > licznik or id < 1) THEN
		RAISE INVALID_NUMBER;
	END IF;
	DBMS_OUTPUT.PUT_LINE('lICZBA JEDNOSTEK: '||licznik);
	FOR x IN 1..licznik LOOP
		l:=0;
		DBMS_OUTPUT.PUT_LINE('ID JEDNOSTKI: '||x);
		FOR tmp IN lol(x) LOOP
		
			IF (tmp.id_jedn < id ) THEN
				DBMS_OUTPUT.PUT_LINE(tmp.nazwisko);
				
				l:=l+1;
			END IF;
			
			
		END LOOP;
		
		IF (l < 2) THEN
				RAISE wyjatek;
			END IF;
		
	END LOOP;
	EXCEPTION
	WHEN wyjatek THEN
	DBMS_OUTPUT.PUT_LINE('WYJATEK jest tylko jedna osoba w jednostce');
	WHEN INVALID_NUMBER THEN
	DBMS_OUTPUT.PUT_LINE('Twoj id jest niepoprawny ');
END;

begin
	lista(5);
end;


//zad7 

CREATE OR REPLACE PROCEDURE zarobki 
IS
CURSOR lol(p NUMBER) IS SELECT pracownicy.nazwisko,jednostki.id_jedn,zatrudnienie.placa_pod,jednostki.nazwa_jedn FROM pracownicy,zatrudnienie,jednostki WHERE pracownicy.id_prac=zatrudnienie.id_prac AND jednostki.id_jedn=zatrudnienie.id_jedn AND jednostki.id_jedn=p;
	tmp lol%ROWTYPE;
	licznik NUMBER;
	
	suma NUMBER;
BEGIN
SELECT count(*) INTO licznik FROM jednostki WHERE id_jedn=id_jedn;

	DBMS_OUTPUT.PUT_LINE('lICZBA JEDNOSTEK: '||licznik);
	FOR x IN 1..licznik LOOP
	
		suma:=0;
		DBMS_OUTPUT.PUT_LINE('ID JEDNOSTKI: '||x);
		FOR tmp IN lol(x) LOOP
		
			
				
				suma:=suma+tmp.placa_pod;
		
			
			
		END LOOP;
		DBMS_OUTPUT.PUT_LINE('Suma dla: '||suma);
	 
		
	END LOOP;



END;

begin
	zarobki();
end;
