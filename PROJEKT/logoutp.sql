CREATE OR REPLACE PROCEDURE logoutp IS
 ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
BEGIN
ciastko := owa_cookie.Get('kluczp');
kod := ciastko.VALS (1);
DELETE FROM webo_sessions2 WHERE valueo=kod;

				  
    htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		<a href="panel"> <p class="navbar-brand"> Panel admina </p></a>

		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Wylogowano</p></h3></br>
				<p class="text-center"><a  href="home">Powrot do strony glownej</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;

END;