
CREATE OR REPLACE PROCEDURE wysylka IS
CURSOR lol IS SELECT sprzedaz.id,ksiazka.isbn,ksiazka.tytul,klient.imie,klient.nazwisko,klient.ulica,klient.miasto,klient.kod_pocztowy,sprzedaz.status FROM ksiazka,klient,sprzedaz WHERE ksiazka.id_sprzedaz=sprzedaz.id AND sprzedaz.id_klienta=klient.id AND status=2;
tmp lol%ROWTYPE;
ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
 licznik NUMBER:=0;
 tmpo webo_sessions2%ROWTYPE;

BEGIN
ciastko := owa_cookie.Get('kluczp');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO licznik FROM webo_sessions2 WHERE valueo=kod;

END IF;

IF (licznik>0) THEN
 
 htp.prn('<!DOCTYPE html>
<html>
<head>

<title>Panel</title>
<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
body, html{
margin:0;
height: 100%;
}
.lol{
	border-radius:0;
	margin-bottom:0;
}
.wiekszy{
height: 100%;
min-height:500px;
padding: 0;
}
.lol{
padding-left:0;
}
.szto{
height: 100%;
}
.bor{
	padding-left:0;
	border-width: 1px;
	border-bottom-style:solid;
}
.lolo{
margin: 0 auto;
}
.nizej{
 margin-top: 10px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		<a href="panel"> <p class="navbar-brand"> Panel admina </p></a>

		</div>
			<a class="btn btn-danger pull-right nizej" href="logoutp">Wyloguj</a>
	  </div>
	</nav>
	<div class="container-fluid lol szto">
	<div class="row szto">
		<div class="col-md-2 wiekszy">
			<div class="well wiekszy">
				<h3 id="tutaj" class="text-center">Ksiazki</h3>
			<hr>
			
		<a href="dodajDostawce"><p class="text-center bor">Dodaj dostawce</p></a>
<a href="edytujDostawce"><p class="text-center bor">Edytuj dostawce</p></a>
<a href="dodajAutora"><p class="text-center bor">Dodaj autora</p></a>
<a href="edytujAutora"><p class="text-center bor">Edytuj autora</p></a>
<a href="dodajMagazyn"><p class="text-center bor">Dodaj Magazyn</p></a>
<a href="edytujMagazyn"><p class="text-center bor">Edytuj Magazyn</p></a>
<a href="dodajWydawnictwo"><p class="text-center bor">Dodaj Wydawnictwo</p></a>
<a href="edytujWydawnictwo"><p class="text-center bor">Edytuj Wydawnictwo</p></a>
<a href="dodajKsiazke"><p class="text-center bor">Dodaj ksiazke</p></a>
<a href="edytujKsiazke"><p class="text-center bor">Edytuj ksiazke</p></a>
<h3 class="text-center">Sprzedaz</h3>
<hr>
<a href="wysylka"><p class="text-center bor">Wysylka</p></a>
<a href="zawieszony"><p class="text-center bor">Zawieszony</p></a>
<a href="wyslany"><p class="text-center bor">Wyslany</p></a>

			</div>
		</div>
		<div class="col-md-10">
			
			<div class="col-md-12">
			
				<h2>Wysylka</h2>
				<table class="table table-bordered">
					<thead>
						<th>ID</th>
						<th>Tytul</th>
						<th>ISBN</th>
						<th>Imie i nazwisko</th>
						<th>Miasto i ulica</th>
						<th>Kod pocztowy</th>
						<th>Status</th>
						<th>Akcja</th>
					</thead>
					<tbody>
						');
						FOR tmp IN lol LOOP
							htp.print('
							<tr>
								<td>
								'||tmp.id||'
								</td>
								<td>
								'||tmp.tytul||'
								</td>
								<td>
								'||tmp.isbn||'
								</td>
								<td>
								'||tmp.imie||' '||tmp.nazwisko||'
								</td>
								<td>
								'||tmp.miasto||' '||tmp.ulica||'
								</td>
								<td>
									'||tmp.kod_pocztowy||'
								</td>
								<td>
									<select id="set'||tmp.id||'" class="form-control">
									  <option value="1">Wyslany</option>
									  <option value="2">Zrealizowany</option>
									  <option value="3">Anulowany</option>
									  <option value="4">Zawieszony</option>
									  <option value="5">Do realizacji</option>
									
									</select>
								</td>
								<td>
									<a href="#" id="clicko'||tmp.id||'" class="btn btn-primary lolo">Zastosuj</a>
								</td>
							</tr>
							');
						
						END LOOP;
						
						htp.print('
					</tbody>
				</table>
			</div>
		</div>
	
	</div>
	</div>
	
  <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script>
	$(document).ready(function(){
		');
		FOR tmp IN lol LOOP
			htp.print('
				var zmienna='||tmp.status||';
				console.log("zmienna: "+zmienna);
				console.log("Ilosc opcji: "+$("#set'||tmp.id||' option").length);
				for(var n=0;n<$("#set'||tmp.id||' option").length;n++){
					console.log(zmienna+"==="+$("#set'||tmp.id||' option").eq(n).val());
					if(zmienna == $("#set'||tmp.id||' option")[n].value){
							console.log("zaszlo");
						$("#set'||tmp.id||' option").eq(n).attr("selected","selected");
					}
				}
				console.log("clicko'||tmp.id||'");
				$("#clicko'||tmp.id||'").on("click",function(){
					var e=document.getElementById("set'||tmp.id||'");
					var op=e.options[e.selectedIndex].value;
					console.log(op);
					
					$.post("status",{sprzedazID: "'||tmp.id||'", statuso: op.toString()}).done(function(){
						
					}).fail(function(){
						
					});
				});
			');
		END LOOP;
		htp.print('
		
	});
  </script>
</body>
</html>');

 ELSE
htp.print('
<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
			 <a href="home"> <p class="navbar-brand"> Panel admina </p></a>

		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Nie masz dostepu zaloguj sie </p></h3></br>
				<p class="text-center"><a  href="pracowniks">Powrot do strony logowania</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>


');

END IF;
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;

END;
/