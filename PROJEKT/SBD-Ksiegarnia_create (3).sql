-- Created by Vertabelo (http://vertabelo.com)-
-- Last modification date: 2017-05-21 10:23:19.812

-- tables
-- Table: autor

CREATE TABLE webo_sessions ( 
	id NUMBER,
	valueo varchar2(4000),
	id_klienta NUMBER) ; 
	
	CREATE TABLE webo_sessions2 ( 
	id NUMBER,
	valueo varchar2(4000),
	id_klienta NUMBER) ; 





CREATE TABLE autor (
    ID number  NOT NULL,
	url varchar2(200),
    imie varchar2(10)  NOT NULL,
    nazwisko varchar2(20)  NOT NULL,
    kraj varchar2(10)  NOT NULL,
	opis varchar2(2000),
    CONSTRAINT autor_pk PRIMARY KEY (ID)
) ;
autorD('http://selkar.pl/img/uploads/Andrzej_Sapkowski.jpg','Andrzej','Sapkowski','PL','polski pisarz fantasy, z wykształcenia ekonomista. Twórca postaci wiedźmina. Jest najczęściej po Lemie tłumaczonym polskim autorem fantastyki');
						autorD('http://scriptshadow.net/wp-content/uploads/2015/04/george-rr-martin.jpg','George','Martin','USA','twórca science fiction i fantasy, zarówno opowiadań jak i powieści, wielokrotny zdobywca nagród Nebula i Hugo oraz wielu innych: World Fantasy Award, Bram Stoker Award');

INSERT INTO autor  VALUES (1,'http://s3.party.pl/newsy/ewa-chodakowska-w-stylizacji-za-10-tysiecy-na-urodzinach-dzien-dobry-tvn-296022-quiz.jpg','Ewa','Chodakowska','PL','Ewa Chodakowska-Kavoukis urodziła się 24 lutego 1982 roku w Sanoku. Jest najbardziej znaną polską trenerką osobistą i specjalistką od kształtowania ciała. Jej programy ćwiczeń i żywienia stały się prawdziwym fenomenem, a charyzma i umiejętności motywacyjne sprawiły, że zaufało jej miliony Polek, odzyskując dobrą formę oraz doskonałą sylwetkę. ');

-- Table: dostawca
CREATE TABLE dostawca (
    ID number  NOT NULL,
	nazwa varchar2(30),
    ulica varchar2(30)  NOT NULL,
    miasto varchar2(20)  NOT NULL,
    kod_pocztowy varchar2(6)  NOT NULL,
    numer_telefonu varchar2(20)  NOT NULL,
    kraj varchar2(10)  NOT NULL,
	nip varchar2(30),
    CONSTRAINT dostawca_pk PRIMARY KEY (ID)
) ;

-- Table: klient
CREATE TABLE klient (
    id number  NOT NULL,
    imie varchar2(30)  NOT NULL,
    nazwisko varchar2(30)  NOT NULL,
    ulica varchar2(30)  NOT NULL,
    miasto varchar2(30)  NOT NULL,
    kod_pocztowy varchar2(10)  NOT NULL,
	mail varchar2(50),
	haslo varchar2(50),
    CONSTRAINT klient_pk PRIMARY KEY (id)
) ;
INSERT INTO klient (id,imie,nazwisko,ulica,miasto,kod_pocztowy,mail,haslo) VALUES (1,'Patriko','Fantastoko','Obosna','Siedlce','08-110','logi','haslo');
-- Table: ksiazka
CREATE TABLE ksiazka (
    id number  NOT NULL,
    isbn varchar2(10)  NOT NULL,
	url varchar2(100),
    tytul varchar2(30)  NOT NULL,
    liczba_stron number  NOT NULL,
    jezyk varchar2(10)  NOT NULL,
    ilosc number  NOT NULL,
    wydanie varchar2(10)  NOT NULL,
    cena number  NOT NULL,
    id_autora number  NOT NULL,
    id_wydawnictwa number  NOT NULL,
    id_dostawcy number  NOT NULL,
    id_pracownika number  NOT NULL,
    id_sprzedaz number,
    id_magazynu number  NOT NULL,
    CONSTRAINT ksiazka_pk PRIMARY KEY (id)
) ;

ksiazkaD('21129797','http://ecsmedia.pl/c/zdrowe-koktajle-w-iext49189898.jpg','Zdrowe koktajle',100,'PL',2,'TWARDE',32,0,0,0);

-- Table: magazyn
CREATE TABLE magazyn (
    ID number  NOT NULL,
    nazwa varchar2(50)  NOT NULL,
    ulica varchar2(30)  NOT NULL,
    miasto varchar2(30)  NOT NULL,
    kod_pocztowy varchar2(10)  NOT NULL,
    numer_telefonu varchar2(20)  NOT NULL,
    kraj varchar2(20)  NOT NULL,
    CONSTRAINT magazyn_pk PRIMARY KEY (ID)
) ;
wydawnictwoD('m2','ulica','miasto','08-110','232233','pl');
-- Table: pracownik
CREATE TABLE pracownik (
    id number  NOT NULL,
	email VARCHAR(50),
	password VARCHAR(50),
    imie varchar2(50)  NOT NULL,
    nazwisko varchar2(50)  NOT NULL,
    ulica varchar2(50)  NOT NULL,
    kod_pocztowy varchar2(10)  NOT NULL,
	miasto varchar2(50),
    pesel varchar2(50)  NOT NULL,
    CONSTRAINT pracownik_pk PRIMARY KEY (id)
) ;

-- Table: sprzedaz
CREATE TABLE sprzedaz (
    id number  NOT NULL,
    id_klienta number  NOT NULL,
	id_ksiazki NUMBER,
    data date  NOT NULL,
	status NUMBER,
    CONSTRAINT sprzedaz_pk PRIMARY KEY (id)
) ;

-- Table: wydawnictwo
CREATE TABLE wydawnictwo (
    id number  NOT NULL,
    nazwa varchar2(30)  NOT NULL,
    ulica varchar2(10)  NOT NULL,
    miasto varchar2(10)  NOT NULL,
    kod_pocztowy varchar2(6)  NOT NULL,
    numer_telefonu varchar2(15)  NOT NULL,
    kraj varchar2(10)  NOT NULL,
    CONSTRAINT wydawnictwo_pk PRIMARY KEY (id)
) ;
INSERT INTO wydawnictwo VALUES (1,'Edipresse Książki','ksiazkiwa','Warszawa','0-100','0700808','PL');
-- foreign keys
-- Reference: ksiazka_autor (table: ksiazka)
ALTER TABLE ksiazka ADD CONSTRAINT ksiazka_autor
    FOREIGN KEY (id_autora)
    REFERENCES autor (ID);

-- Reference: ksiazka_dostawca (table: ksiazka)
ALTER TABLE ksiazka ADD CONSTRAINT ksiazka_dostawca
    FOREIGN KEY (id_dostawcy)
    REFERENCES dostawca (ID);

-- Reference: ksiazka_magazyn (table: ksiazka)
ALTER TABLE ksiazka ADD CONSTRAINT ksiazka_magazyn
    FOREIGN KEY (id_magazynu)
    REFERENCES magazyn (ID);

-- Reference: ksiazka_sprzedaz (table: ksiazka)
ALTER TABLE ksiazka ADD CONSTRAINT ksiazka_sprzedaz
    FOREIGN KEY (id_sprzedaz)
    REFERENCES sprzedaz (id);

-- Reference: ksiazka_wydawnictwo (table: ksiazka)
ALTER TABLE ksiazka ADD CONSTRAINT ksiazka_wydawnictwo
    FOREIGN KEY (id_wydawnictwa)
    REFERENCES wydawnictwo (id);

-- Reference: pracownik_ksiazka (table: ksiazka)
ALTER TABLE ksiazka ADD CONSTRAINT pracownik_ksiazka
    FOREIGN KEY (id_pracownika)
    REFERENCES pracownik (id);

-- Reference: sprzedaz_Klient (table: sprzedaz)
ALTER TABLE sprzedaz ADD CONSTRAINT sprzedaz_Klient
    FOREIGN KEY (id_pracownika)
    REFERENCES klient (id);

-- Reference: sprzedaz_pracownik (table: sprzedaz)
ALTER TABLE sprzedaz ADD CONSTRAINT sprzedaz_pracownik
    FOREIGN KEY (id_pracownika)
    REFERENCES pracownik (id);

-- End of file.

