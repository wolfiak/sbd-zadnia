CREATE OR REPLACE PROCEDURE sprzedazo(klientid IN NUMBER, ksiazkaid IN NUMBER) IS
licznik NUMBER:=0;
li NUMBER:=0;
data DATE;
ks ksiazka%ROWTYPE;
 ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
 liczniko NUMBER:=0;
 tmpo webo_sessions%ROWTYPE;
BEGIN
ciastko := owa_cookie.Get('klucz');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO liczniko FROM webo_sessions WHERE valueo=kod;

END IF;



IF (liczniko>0) THEN
SELECT SYSDATE INTO data FROM DUAL;
SELECT count(*) INTO li FROM sprzedaz;
IF (li=0) THEN
	licznik:=0;
	ELSE
	SELECT MAX(id) INTO licznik FROM sprzedaz;
	licznik:=licznik+1;
END IF;


INSERT INTO sprzedaz VALUES (licznik,klientid,ksiazkaid,data,5);
UPDATE ksiazka SET ilosc=ilosc-1 WHERE id=ksiazkaid;
UPDATE ksiazka SET id_sprzedaz=licznik WHERE id=ksiazkaid;
SELECT * INTO ks FROM ksiazka WHERE id=ksiazkaid;
 htp.print('<!DOCTYPE html>
<html>
<head>

<title>Rejestracja</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Kupiles '||ks.tytul||' </p></h3></br>
				<p class="text-center"><a  href="home">Powrot do Sklepu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
 ELSE
htp.print('
<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <a href="home"> <p class="navbar-brand"> Home </p></a>

		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Nie masz dostepu zaloguj sie </p></h3></br>
				<p class="text-center"><a  href="home">Powrot do strony glownej</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>


');

END IF;
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;
END;