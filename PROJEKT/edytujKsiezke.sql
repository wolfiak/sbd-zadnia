
CREATE OR REPLACE PROCEDURE edytujKsiazke IS
ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
 licznik NUMBER:=0;
 tmpo webo_sessions2%ROWTYPE;
 CURSOR lol IS SELECT * FROM ksiazka;
 tmp lol%ROWTYPE;
BEGIN
ciastko := owa_cookie.Get('kluczp');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO licznik FROM webo_sessions2 WHERE valueo=kod;

END IF;

IF (licznik>0) THEN

 htp.prn('<!DOCTYPE html>
<html>
<head>

<title>Panel</title>
<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
body, html{
margin:0;
height: 100%;
}
.lol{
	border-radius:0;
	margin-bottom:0;
}
.wiekszy{
height: 100%;
min-height:500px;
padding: 0;
}
.lol{
padding-left:0;
}
.szto{
height: 100%;
}
.bor{
	padding-left:0;
	border-width: 1px;
	border-bottom-style:solid;
}
.lolo{
margin: 0 auto;
}
.nizej{
 margin-top: 10px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Panel admina </p>
		</div>
			<a class="btn btn-danger pull-right nizej" href="logoutp">Wyloguj</a>
	  </div>
	</nav>
	<div class="container-fluid lol szto">
	<div class="row szto">
		<div class="col-md-2 wiekszy">
			<div class="well wiekszy">
					<h3 id="tutaj" class="text-center">Ksiazki</h3>
			<hr>
			
			<a href="dodajDostawce"><p class="text-center bor">Dodaj dostawce</p></a>
			<a href="dodajAutora"><p class="text-center bor">Dodaj autora</p></a>
			<a href="dodajMagazyn"><p class="text-center bor">Dodaj Magazyn</p></a>
			<a href="dodajWydawnictwo"><p class="text-center bor">Dodaj Wydawnictwo</p></a>
			<a href="dodajKsiazke"><p class="text-center bor">Dodaj ksiazke</p></a>
			
			
			<h3 class="text-center">Sprzedaz</h3>
			<hr>
			<a href="wysylka"><p class="text-center bor">Wysylka</p></a>
			<a href="zawieszony"><p class="text-center bor">Zawieszony</p></a>
			<a href="wyslany"><p class="text-center bor">Wyslany</p></a>
			
			</div>
		</div>
		<div class="col-md-10">
			
			
			
				<h2>Wybierz dostawce do edycji</h2>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ID</th>
							<th>ISBN</th>
							<th>url</th>
							<th>Tytul</th>
							<th>Liczba stron</th>
							<th>Jezyk</th>
							<th>Ilosc</th>
							<th>Wydanie</th>
							<th>Cena</th>
							<th>ID autora</th>
							<th>ID wydawnictwa</th>
							<th>ID dostawcy</th>
						
							<th>ID sprzedaz</th>
							<th>ID magazynu</th>
							<th>Edytuj</th>
							<th>Usun</th>
						</tr>
					</thead>
					<tbody>
					');
					FOR tmp IN lol LOOP
						htp.print('
							
							<tr>
								<td>'||tmp.id||'</td>
								<td>'||tmp.isbn||'</td>
								<td>'||tmp.url||'</td>
								<td>'||tmp.tytul||'</td>
								<td>'||tmp.liczba_stron||'</td>
								<td>'||tmp.jezyk||'</td>
								<td>'||tmp.ilosc||'</td>
								<td>'||tmp.wydanie||'</td>
								<td>'||tmp.cena||'</td>
								<td>'||tmp.id_autora||'</td>
								<td>'||tmp.id_wydawnictwa||'</td>
								<td>'||tmp.id_dostawcy||'</td>
							
								<td>'||tmp.id_sprzedaz||'</td>
								<td>'||tmp.id_magazynu||'</td>
								<td><a href="edytujKsiazka?ido='||tmp.id||'" class="btn btn-success btn-block">Edytuj</a></td>
								<td><a href="usunKsiazka?ido='||tmp.id||'" class="btn btn-danger btn-block">Usun</a></td>
							</tr>
						');				
					
					END LOOP;
					htp.print('
					</tbody>
					
				</table>
			
			
		</div>
	
	</div>
	</div>
	
 
</body>
</html>');
 
ELSE
htp.print('
<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Nie masz dostepu zaloguj sie </p></h3></br>
				<p class="text-center"><a  href="pracowniks">Powrot do strony logowania</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>


');

END IF;
END;
/