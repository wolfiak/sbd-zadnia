CREATE OR REPLACE PROCEDURE profil(ido IN NUMBER ) IS
 tmp klient%ROWTYPE;
 CURSOR lol(p NUMBER) IS SELECT ksiazka.tytul,ksiazka.cena,autor.imie,autor.nazwisko,sprzedaz.id FROM ksiazka,autor,sprzedaz,klient WHERE sprzedaz.id_klienta=p AND ksiazka.id=sprzedaz.id_ksiazki AND ksiazka.id_autora=autor.ID and sprzedaz.id=sprzedaz.id;
 tmpo lol%ROWTYPE;
 licznik NUMBER:=0;
 licz NUMBER:=0;
  ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
 liczniko NUMBER:=0;
 tmpo webo_sessions%ROWTYPE;
 BEGIN
 ciastko := owa_cookie.Get('klucz');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO liczniko FROM webo_sessions WHERE valueo=kod;

END IF;



IF (liczniko>0) THEN
SELECT count(*) INTO licznik FROM sprzedaz WHERE id_klienta=ido;
SELECT * INTO tmp FROM klient WHERE id=ido;
    htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
.bor{
	border-width: 1px;
	border-right-style:  solid;
}
.odstep{
margin-right: 10px;
}
.wiekszy{
margin-top: 10px;
font-size: 20px;
}
.wieksz{
font-size: 20px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		<a href="home"> <p class="navbar-brand"> Ksiegarnia </p></a>
		
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="col-md-3 bor">
			<h3>Dane klienta</h3>
			<p class="wiekszy"><span class="glyphicon glyphicon-user odstep">  </span>'||tmp.imie||'  '||tmp.nazwisko||'</p>
			 <address class="wieksz">
			<span class="glyphicon glyphicon-home odstep"></span><strong>Adres</strong><br>
			'||tmp.ulica||'<br>
			'||tmp.miasto||' '||tmp.kod_pocztowy||'<br>
		
			</address>
			<p class="wiekszy"><span class="glyphicon glyphicon-envelope odstep"></span> '||tmp.mail||'</p>
		</div>
		<div class="col-md-9">
			<h3>Zamowienia</h3>
			
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ID sprzedazy</th>
						<th>Tytul</th>
						<th>Autor</th>
						<th>Cena</th>
					</tr>
				</thead>
				<tbody>
				');
				
				FOR tmpo IN lol(tmp.id) LOOP
					IF (licznik=licz) THEN
						EXIT;
					END IF;
					htp.print('
						<tr>
							<td>'||tmpo.id||'</td>
							<td>'||tmpo.tytul||'</td>
							<td>'||tmpo.imie||' '||tmpo.nazwisko||'</td>
							<td>'||tmpo.cena||'</td>
						
						</tr>
					
					');
				
					licz:=licz+1;
				END LOOP;
				
htp.print('			</tbody></table>
		</div>
		

	
	</div>
	</div>
</body>
</html>');
ELSE
htp.print('
<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Nie masz dostepu zaloguj sie </p></h3></br>
				<p class="text-center"><a  href="home">Powrot do strony glownej</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>


');

END IF;
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;
END;
