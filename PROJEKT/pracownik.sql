CREATE OR REPLACE PROCEDURE pracowniks IS

 BEGIN

    htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
	margin-bottom:0;
}
.bor{
	border-width: 1px;
	border-right-style:  solid;
}
.odstep{
margin-right: 10px;
}
.wiekszy{
margin-top: 10px;
font-size: 20px;
}
.wieksz{
font-size: 20px;
}

.bluo{
background: #2196F3;
min-height: 470px;
color:white;
}
.redo{
background: #f44336;
min-height: 470px;
color:white;
}
.jumbotron{
color:#ECEFF1;
background:#212121;
min-height:700px;
}
.navo{
background: #9E9E9E;
}
.vader{
margin-top: 9px;
width: 45%;
}
.luke{
width:25%;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol navo">
	  <div class="container-fluid">
		<div class="navbar-header">
		<a href="home"> <p class="navbar-brand"> Ksiegarnia </p></a>
		
		</div>
	  </div>
	</nav>

	
	<div class="jumbotron">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<h1 class="text-center">Pick a side</h1>
				</div>
				<a href="logowaniep">
				<div class="col-md-6 redo">
					<h2 class="text-center">Zaloguj</h2>
					<img class="vader" src="https://vignette3.wikia.nocookie.net/murderseries/images/d/dc/Vader.png/revision/latest/scale-to-width-down/476?cb=20150612203551">
				</div>
				</a>
				<a href="rejestracjap">
				<div class="col-md-6 bluo">
					<h2 class="text-center">Aplikuj</h2>
					<img class="pull-right luke" src="http://www.pngmart.com/files/2/Luke-Skywalker-PNG-File.png">
				</div>
				</a>
			</div>
		</div>
	</div>
</body>
</html>');

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;
END;