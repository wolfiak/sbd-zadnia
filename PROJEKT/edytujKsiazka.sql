
CREATE OR REPLACE PROCEDURE edytujKsiazka(ido IN NUMBER) IS
	CURSOR autor IS SELECT * FROM autor;
	CURSOR wyda IS SELECT * FROM wydawnictwo;
	CURSOR mag IS SELECT * FROM magazyn;
    CURSOR dos IS SELECT * FROM dostawca;
	tmp autor%ROWTYPE;
	tmp2 wydawnictwo%ROWTYPE;
	tmp3 magazyn%ROWTYPE;
	ciastko owa_cookie.cookie;
    tmp4 ksiazka%ROWTYPE;
    tmp5 dostawca%ROWTYPE;
 kod VARCHAR2(30);
 liczniko NUMBER:=0;
 tmpo webo_sessions2%ROWTYPE;

BEGIN
ciastko := owa_cookie.Get('kluczp');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO liczniko FROM webo_sessions2 WHERE valueo=kod;

END IF;

IF (liczniko>0) THEN
 SELECT * INTO tmp4 FROM ksiazka WHERE id=ido;
 htp.prn('<!DOCTYPE html>
<html>
<head>

<title>Panel</title>
<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
body, html{
margin:0;
height: 100%;
}
.lol{
	border-radius:0;
	margin-bottom:0;
}
.wiekszy{
height: 100%;
min-height:500px;
padding: 0;
}
.lol{
padding-left:0;
}
.szto{
height: 100%;
}
.bor{
	padding-left:0;
	border-width: 1px;
	border-bottom-style:solid;
}
.lolo{
margin: 0 auto;
}
.od{
margin-top: 66px;
}
.tutaj{
	margin-top: 5px;
	padding: 10px;
	display: none;
}
.nizej{
 margin-top: 10px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Panel admina </p>
		</div>
			<a class="btn btn-danger pull-right nizej" href="logoutp">Wyloguj</a>
	  </div>
	</nav>
	<div class="container-fluid lol szto">
	<div class="row szto">
		<div class="col-md-2 wiekszy">
			<div class="well wiekszy">
				<h3 id="tutaj" class="text-center">Ksiazki</h3>
			<hr>
			
			<a href="dodajDostawce"><p class="text-center bor">Dodaj dostawce</p></a>
			<a href="dodajAutora"><p class="text-center bor">Dodaj autora</p></a>
			<a href="dodajMagazyn"><p class="text-center bor">Dodaj Magazyn</p></a>
			<a href="dodajWydawnictwo"><p class="text-center bor">Dodaj Wydawnictwo</p></a>
			<a href="dodajKsiazke"><p class="text-center bor">Dodaj ksiazke</p></a>
			
			
			<h3 class="text-center">Sprzedaz</h3>
			<hr>
			<a href="wysylka"><p class="text-center bor">Wysylka</p></a>
			<a href="zawieszony"><p class="text-center bor">Zawieszony</p></a>
			<a href="wyslany"><p class="text-center bor">Wyslany</p></a>
			
			</div>
		</div>
		<div class="col-md-10">
			<div  class="bg-success tutaj"> 
				
				</div>
			<div class="col-md-6">
			
				<h2 >Edytuj Ksiazke</h2>
				
	<input style="display: none;" type="text" class="form-control" id="ido" name="ido" value="'||tmp4.id||'">			
  <div class="form-group">
    <label for="isbnl">ISBN</label>
    <input type="text" class="form-control" id="isbnl" name="isbnl" value="'||tmp4.isbn||'">
  </div>
    <div class="form-group">
    <label for="urll">Url</label>
    <input type="text" class="form-control" id="urll" name="urll" value="'||tmp4.url||'">
  </div>
  <div class="form-group">
    <label for="tytull">Tytul</label>
    <input type="text" class="form-control" id="tytull" name="tytull" value="'||tmp4.tytul||'" >
  </div>
	
  <div class="form-group">
    <label for="liczbastronl">Liczba stron</label>
    <input type="text" class="form-control" id="liczbastronl" name="liczbastronl" value="'||tmp4.liczba_stron||'" >
  </div>
    <div class="form-group">
    <label for="jezykl">Jezyk</label>
    <input type="text" class="form-control" id="jezykl" name="jezykl" value="'||tmp4.jezyk||'">
  </div>
       <div class="form-group">
    <label for="iloscl">Ilosc</label>
    <input type="text" class="form-control" id="iloscl" name="iloscl" value="'||tmp4.ilosc||'">
  </div>
     
	    <div class="form-group">
    <label for="wydaniel">Wydanie</label>
    <input type="text" class="form-control" id="wydaniel" name="wydaniel" value="'||tmp4.wydanie||'" >
  </div>
  
     <div class="form-group">
    <label for="cenal">Cena</label>
    <input type="text" class="form-control" id="cenal" name="cenal" value="'||tmp4.cena||'">
  </div>
 


	
  

			
			</div>
			<div class="col-md-6 od">
			    <div class="form-group">
				<label for="autorid">Autor</label>
				<select class="form-control" id="autorid" name="autorid">
				');
				FOR tmp IN autor LOOP
                IF tmp.id=tmp4.id_autora THEN
				htp.print('
				<option selected="selected" value="'||tmp.id||'">
				'||tmp.imie||' '||tmp.nazwisko||'
				</option>
		
				');
                ELSE
                	htp.print('
				<option  value="'||tmp.id||'">
				'||tmp.imie||' '||tmp.nazwisko||'
				</option>
		
				');
                END IF;
		
	END LOOP;
	htp.print('
	</select>
      </div>
				 <div class="form-group">
				<label for="wydid">Wydawnictwo</label>
				<select class="form-control" id="wydid" name="wydid">
				');
				FOR tmp2 IN wyda LOOP
                IF tmp2.id=tmp4.id_wydawnictwa THEN
					htp.print('
					<option selected="selected" value="'||tmp2.id||'">
						'||tmp2.nazwa||' 
					</option>
		
					');
                ELSE
                    htp.print('
					<option value="'||tmp2.id||'">
						'||tmp2.nazwa||' 
					</option>
		
					');
                END IF;
		
				END LOOP;
				htp.print('
				</select>
				</div>
				
					 <div class="form-group">
				<label for="magid">Magazyn</label>
				<select class="form-control" id="magid" name="magid">
				');
				FOR tmp3 IN mag LOOP
                IF tmp3.id=tmp4.id_magazynu THEN
					htp.print('
					<option selected="selected" value="'||tmp3.ID||'">
						'||tmp3.nazwa||' 
					</option>
		
					');
                 ELSE
                 	htp.print('
					<option value="'||tmp3.ID||'">
						'||tmp3.nazwa||' 
					</option>
		
					');
                END IF;

                
				END LOOP;
				htp.print('
				</select>
				</div>

				 <div class="form-group">
				<label for="dosid">Dostawca</label>
				<select class="form-control" id="dosid" name="dosid">
				');
				FOR tmp5 IN dos LOOP
                 IF tmp5.id=tmp4.id_magazynu THEN
					htp.print('
					<option  selected="selected" value="'||tmp5.ID||'">
						'||tmp5.nazwa||' 
					</option>
		
					');
                 ELSE
                    	htp.print('
					<option value="'||tmp5.ID||'">
						'||tmp5.nazwa||' 
					</option>
		
					');
                  END IF;
	
		
				END LOOP;
				htp.print('
				</select>
				</div>

			
				<a id="clicko" class="btn btn-success pull-right">Potwiedz</a>
			</div>
			
			
		</div>
	
	</div>
	</div>
	<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
  <script>
	$(document).ready(function(){
	
		$("#clicko").on("click",function(){
                    var ido=document.getElementById("ido");
					var isbnl=document.getElementById("isbnl");
					var urll=document.getElementById("urll");
					var tytull=document.getElementById("tytull");
					var liczbastronl=document.getElementById("liczbastronl");
					var jezykl=document.getElementById("jezykl");
					var tytull=document.getElementById("tytull");
					var iloscl=document.getElementById("iloscl");
					var wydaniel=document.getElementById("wydaniel");
					var cenal=document.getElementById("cenal");
					var tmp=document.getElementById("autorid");
					var autor=tmp.options[tmp.selectedIndex].value;
					var tmp2=document.getElementById("wydid");
					var wyd=tmp2.options[tmp2.selectedIndex].value;
					var tmp3=document.getElementById("magid");
					var mag=tmp3.options[tmp3.selectedIndex].value;
                    var tmp5=document.getElementById("dosid");
					var dos=tmp5.options[tmp5.selectedIndex].value;
					console.log(mag);
	
					$.post("eksiazkaD",{ido: ido.value,isbnl: isbnl.value, urll: urll.value, tytull: tytull.value, liczbastronl: liczbastronl.value, jezykl: jezykl.value,iloscl: iloscl.value, wydaniel: wydaniel.value,cenal: cenal.value, autorid: autor,wydid: wyd,dosid: dos,magid: mag }).done(function(){
						console.log("poszlo");
						$(".tutaj").append("<span><h2>Ksiazka edytowana wroc do panelu </h2></span><a href=panel>Wroc</a>").fadeIn();
					}).fail(function(err){
				
						alert("nope: "+err);
					});
				});
	
	
	});
  </script>
 
</body>
</html>');
 ELSE
htp.print('
<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Nie masz dostepu zaloguj sie </p></h3></br>
				<p class="text-center"><a  href="pracowniks">Powrot do strony logowania</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>


');

END IF;

END;
/