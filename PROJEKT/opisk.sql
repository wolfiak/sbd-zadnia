CREATE OR REPLACE PROCEDURE opisk(ido IN NUMBER) IS
	CURSOR lol(p NUMBER) IS SELECT ksiazka.tytul,ksiazka.isbn,ksiazka.url,ksiazka.liczba_stron,ksiazka.jezyk,ksiazka.ilosc,ksiazka.wydanie,ksiazka.cena,autor.imie,autor.nazwisko,autor.kraj,wydawnictwo.nazwa,autor.url as url2,autor.opis FROM ksiazka,wydawnictwo,autor WHERE ksiazka.id_autora=autor.ID AND  ksiazka.id=p;
	tmp lol%ROWTYPE;
	tmpa autor%ROWTYPE;
	licznik1 NUMBER:=0;
	licznik2 NUMBER:=0;
BEGIN

 htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
.bor{
	border-width: 1px;
	border-right-style:  solid;
}
.odstep{
margin-right: 10px;
}
.wiekszy{
margin-top: 10px;
font-size: 20px;
}
.wieksz{
font-size: 20px;
}
.troche{
font-size: 19px;
margin-top: 15%;
}
.some{
margin-bottom: 10px;
}
.wielkosc{
height: 250px;
}

</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		<a href="home"> <p class="navbar-brand"> Ksiegarnia </p></a>
		
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
	');
	FOR tmp IN lol(ido) LOOP
	IF (licznik1=0) THEN
	htp.print('
		
		<div class="col-md-3 bor">
			<img class="thumbnail" src="'||tmp.url||'">
			
			<h2 class="text-center">'||tmp.tytul||'</h2>
		</div>
		<div class="col-md-9">
		 
		  
			
			<table class="table table-striped wieksz">
				<tbody>
					<tr>
						<td>ISBN</td>
						<td>'||tmp.isbn||'</td>
					</tr>
					<tr>
						<td>Liczba stron</td>
						<td>'||tmp.liczba_stron||'</td>
						
					</tr>
					<tr>
						<td>Jezyk</td>
						<td>'||tmp.jezyk||'</td>
					</tr>
					<tr>
						<td>Ilosc</td>
						<td>'||tmp.ilosc||'</td>
					</tr>
					<tr>
						<td>Wydanie</td>
						<td>'||tmp.wydanie||'</td>
					</tr>
					<tr>
						<td>Wydawnictwo</td>
						<td>'||tmp.nazwa||'</td>
					</tr>
				
				</tbody>
			</table>
		</div>
	
	');
		licznik1:=licznik1+1;
	END IF;
	END LOOP;
htp.print('	
	</div>
	<div class="row">
		
		
			');
			FOR tmp IN lol(ido) LOOP
			IF (licznik2=0) THEN
	htp.print('
		<hr>
		<div class="col-md-3">
	
		 <h2 class="some">Autor</h2>
			<img class="img-circle wielkosc" src="'||tmp.url2||'">
			
			
			<h2 class="text-center">'||tmp.imie||' '||tmp.nazwisko||'</h2>
		
		</div>
		<div class="col-md-9">
		 <p class="troche">'||tmp.opis||'</p>
		</div>
		
	');
	licznik2:=licznik2+1;
	END IF;
	END LOOP;
htp.print('
	
	</div>
	</div>
</body>
</html>');
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;


END;