CREATE OR REPLACE PACKAGE BODY pakiet AS
 FUNCTION alerty
RETURN NUMBER IS
    CURSOR ks IS SELECT * FROM ksiazka;
    tmp ks%ROWTYPE;
    licznik NUMBER:=0;
    ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
 liczniko NUMBER:=0;
 tmpo webo_sessions2%ROWTYPE;
BEGIN
ciastko := owa_cookie.Get('kluczp');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO liczniko FROM webo_sessions2 WHERE valueo=kod;

END IF;

IF (liczniko>0) THEN
    FOR tmp IN ks LOOP
        IF (tmp.ilosc < 5 AND tmp.ilosc > 0) THEN
            licznik:=licznik+1;

        END IF;

    END LOOP;
RETURN licznik;
END IF;
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;
END alerty;

 FUNCTION krytyczne
RETURN NUMBER IS
    CURSOR ks IS SELECT * FROM ksiazka;
    tmp ks%ROWTYPE;
    licznik NUMBER:=0;
    ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
 liczniko NUMBER:=0;
 tmpo webo_sessions2%ROWTYPE;
BEGIN
ciastko := owa_cookie.Get('kluczp');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO liczniko FROM webo_sessions2 WHERE valueo=kod;

END IF;

IF (liczniko>0) THEN
    FOR tmp IN ks LOOP
        IF (tmp.ilosc = 0 ) THEN
            licznik:=licznik+1;

        END IF;

    END LOOP;
RETURN licznik;
END IF;
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;
END krytyczne;


 PROCEDURE widok IS
ciastko owa_cookie.cookie;
 kod VARCHAR2(30);
 licznik NUMBER:=0;
 tmpo webo_sessions2%ROWTYPE;
 liczba NUMBER:=0;
 liczba2 NUMBER:=0;
 CURSOR ale IS SELECT * FROM ksiazka;
 tmp ale%ROWTYPE;
 tmp2 dostawca%ROWTYPE;
BEGIN
ciastko := owa_cookie.Get('kluczp');
IF (ciastko.num_vals = 0 ) THEN
DBMS_OUTPUT.PUT_LINE('koniec');
ELSE
kod := ciastko.VALS (1);
SELECT count(*) INTO licznik FROM webo_sessions2 WHERE valueo=kod;

END IF;

IF (licznik>0) THEN
 liczba:=alerty();
 liczba2:=krytyczne();
 htp.prn('<!DOCTYPE html>
<html>
<head>

<title>Panel</title>
<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
body, html{
margin:0;
height: 100%;
}
.lol{
	border-radius:0;
	margin-bottom:0;
}
.wiekszy{
height: 100%;
min-height:500px;
padding: 0;
}
.lol{
padding-left:0;
}
.szto{
height: 100%;
}
.bor{
	padding-left:0;
	border-width: 1px;
	border-bottom-style:solid;
}
.lewaGranica{
    border-width: 1px;
	border-right-style:solid;
}
.lolo{
margin: 0 auto;
}
.nizej{
 margin-top: 10px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		<a href="panel"> <p class="navbar-brand"> Panel admina </p></a>
		</div>
			<a class="btn btn-danger pull-right nizej" href="logoutp">Wyloguj</a>
	  </div>
	</nav>
	<div class="container-fluid lol szto">
	<div class="row szto">
		<div class="col-md-2 wiekszy">
			<div class="well wiekszy">
					<h3 id="tutaj" class="text-center">Ksiazki</h3>
			<hr>
			
		<a href="dodajDostawce"><p class="text-center bor">Dodaj dostawce</p></a>
<a href="edytujDostawce"><p class="text-center bor">Edytuj dostawce</p></a>
<a href="dodajAutora"><p class="text-center bor">Dodaj autora</p></a>
<a href="edytujAutora"><p class="text-center bor">Edytuj autora</p></a>
<a href="dodajMagazyn"><p class="text-center bor">Dodaj Magazyn</p></a>
<a href="edytujMagazyn"><p class="text-center bor">Edytuj Magazyn</p></a>
<a href="dodajWydawnictwo"><p class="text-center bor">Dodaj Wydawnictwo</p></a>
<a href="edytujWydawnictwo"><p class="text-center bor">Edytuj Wydawnictwo</p></a>
<a href="dodajKsiazke"><p class="text-center bor">Dodaj ksiazke</p></a>
<a href="edytujKsiazke"><p class="text-center bor">Edytuj ksiazke</p></a>
<h3 class="text-center">Sprzedaz</h3>
<hr>
<a href="wysylka"><p class="text-center bor">Wysylka</p></a>
<a href="zawieszony"><p class="text-center bor">Zawieszony</p></a>
<a href="wyslany"><p class="text-center bor">Wyslany</p></a>

			
			</div>
		</div>
		<div class="col-md-10">
			
			<div class="col-md-6">
			
				<h2>Alerty <span class="badge">'||liczba||'</span></h2>
			');
                FOR tmp IN ale LOOP
                    IF (tmp.ilosc < 5 AND tmp.ilosc > 0) THEN
                        SELECT * INTO tmp2 FROM dostawca WHERE ID=tmp.id_dostawcy;
                        htp.print('
                     
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title">Koniczy sie  zapas '||tmp.tytul||'</h3>
                            </div>
                            <div class="panel-body">
                               <div class="col-md-6 lewaGranica">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                ISBN
                                                </td>
                                                <td>
                                                '||tmp.isbn||'
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                               Ilosc
                                                </td>
                                                <td>
                                               '||tmp.ilosc||'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                               Wydanie
                                                </td>
                                                <td>
                                               '||tmp.wydanie||'
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                              Cena
                                                </td>
                                                <td>
                                              '||tmp.cena||'
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                         
                            
                             
                              </div>
                              <div class="col-md-6">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                Nazwa
                                                </td>
                                                <td>
                                                '||tmp2.nazwa||'
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                               NIP
                                                </td>
                                                <td>
                                               '||tmp2.nip||'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                               Numer telefonu
                                                </td>
                                                <td>
                                               '||tmp2.numer_telefonu||'
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                              Kraj
                                                </td>
                                                <td>
                                              '||tmp2.kraj||'
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                         
                            
                             
                              </div>
         
                            </div>
                                                                                     <ul class="list-group">
    <li class="list-group-item"><a href="edytujKsiazka?ido='||tmp.id||'" class="btn btn-warning btn-block">Edytuj ksiazke</a></li>
   
  </ul>
                        </div>
                        
                        ');
                    END IF;


                END LOOP;

            htp.print('
			
			</div>
            <div class="col-md-6">
                <h2>Krytyczne <span class="badge">'||liczba2||'</span></h2>
                ');
                FOR tmp IN ale LOOP
                    IF (tmp.ilosc = 0 ) THEN
                        SELECT * INTO tmp2 FROM dostawca WHERE ID=tmp.id_dostawcy;
                        htp.print('
                     
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">Koniec zapasu  '||tmp.tytul||'</h3>
                            </div>
                            <div class="panel-body">
                               <div class="col-md-6 lewaGranica">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                ISBN
                                                </td>
                                                <td>
                                                '||tmp.isbn||'
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                               Ilosc
                                                </td>
                                                <td>
                                               '||tmp.ilosc||'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                               Wydanie
                                                </td>
                                                <td>
                                               '||tmp.wydanie||'
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                              Cena
                                                </td>
                                                <td>
                                              '||tmp.cena||'
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                         
                            
                             
                              </div>
                              <div class="col-md-6">
                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td>
                                                Nazwa
                                                </td>
                                                <td>
                                                '||tmp2.nazwa||'
                                                </td>
                                            </tr>
                                             <tr>
                                                <td>
                                               NIP
                                                </td>
                                                <td>
                                               '||tmp2.nip||'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                               Numer telefonu
                                                </td>
                                                <td>
                                               '||tmp2.numer_telefonu||'
                                                </td>
                                            </tr>
                                              <tr>
                                                <td>
                                              Kraj
                                                </td>
                                                <td>
                                              '||tmp2.kraj||'
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                         
                            
                             
                              </div>
 
                            </div>
                                                                                     <ul class="list-group">
    <li class="list-group-item"><a href="edytujKsiazka?ido='||tmp.id||'" class="btn btn-danger btn-block">Edytuj ksiazke</a></li>
   
  </ul>
                        </div>
                        
                        ');
                    END IF;


                END LOOP;

            htp.print('
            </div>
		</div>
	
	</div>
	</div>
	
 
</body>
</html>');
 
ELSE
htp.print('
<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-center"> Nie masz dostepu zaloguj sie </p></h3></br>
				<p class="text-center"><a  href="pracowniks">Powrot do strony logowania</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>


');

END IF;
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END widok;


END;

 

END pakiet;
