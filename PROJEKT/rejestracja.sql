CREATE OR REPLACE PROCEDURE rejestracja IS

BEGIN
htp.htmlOpen;
htp.headOpen;

htp.title('Moja pierwsza procedura');
htp.print('<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >');
htp.print('<style> 
.wielki{
	font-size: 32px;
}
.odstep{
	margin-bottom:10px;
}

</style>');
htp.headClose;
htp.bodyOpen;
htp.print('<nav class="navbar navbar-inverse lol">
  <div class="container-fluid">
    <div class="navbar-header">
     <a href="home"> <p class="navbar-brand"> Home </p></a>

    </div>
  </div>
</nav>');
htp.print('<div class="container">');
htp.print('<div class="row">');
htp.print('
		<div class="col-sm-6">
	<form method="POST" action="dod">
  <div class="form-group">
    <label for="mail">Adres Email</label>
    <input type="text" class="form-control" id="mail" name="mail">
  </div>
  <div class="form-group">
    <label for="pass">Haslo</label>
    <input type="text" class="form-control" id="pass" name="pass" >
  </div>
	
  <div class="form-group">
    <label for="imie">Imie</label>
    <input type="text" class="form-control" id="imie" name="imie" >
  </div>
    <div class="form-group">
    <label for="nazw">Nazwisko</label>
    <input type="text" class="form-control" id="nazw" name="nazw">
  </div>
      <div class="form-group">
    <label for="ulica">Ulica</label>
    <input type="text" class="form-control" id="ulica" name="ulica">
  </div>
      <div class="form-group">
    <label for="miasto">Miasto</label>
    <input type="text" class="form-control" id="miasto" name="miasto">
  </div>
     <div class="form-group">
    <label for="kod">Kod pocztowy</label>
    <input type="text" class="form-control" id="kod" name="kod">
  </div>
	
  <input type="submit" value="Potwierdz" class="btn btn-default pull-right"></input>
</form>
</div>
');
	htp.print('</div>');
htp.print('</div>');
htp.bodyClose;
htp.htmlClose;
EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;
END;
/
