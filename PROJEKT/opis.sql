CREATE OR REPLACE PROCEDURE  opis (idl NUMBER) IS
	
	CURSOR lol(p NUMBER) IS SELECT * FROM autor where ID=p;
	tmp lol%ROWTYPE;
	CURSOR ks IS SELECT * FROM ksiazka WHERE id_autora=idl;
	tmp2 ks%ROWTYPE;
	licznik NUMBER:=1;
	
BEGIN

htp.htmlOpen;
htp.headOpen;

htp.title('Moja pierwsza procedura');
htp.print('<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >');
htp.print('<style> 
.wielki{
	font-size: 32px;
}
.odstep{
	margin-bottom:10px;
}
.wielkosc{
 height: 300px;
}

</style>');
htp.headClose;
htp.bodyOpen;
htp.print('<nav class="navbar navbar-inverse lol">
  <div class="container-fluid">
    <div class="navbar-header">
   <a href="home"> <p class="navbar-brand"> Home </p></a>

    </div>
  </div>
</nav>');
htp.print('<div class="container">');
htp.print('<div class="row">');
	FOR tmp IN lol(idl) LOOP
		htp.print('<div class="col-sm-3">
			
			<img id="image1" src="'||tmp.url||'" class="img-circle wielkosc" >
				
			
		
			<span class="wielki">'||tmp.imie||'</span> <span class="wielki">'||tmp.nazwisko||'</span>
		
		</div>
		');
	
	END LOOP;
		htp.print('</div>');
	htp.print('<div class="row">');
	
	htp.print('<div class="col-sm-9 col-sm-offset-3"> <h2>Ksiazki autora w ofercie</h2>');
	htp.print('<table class="table table-striped">');
	htp.print('<thead>
		<tr>
			<th>Nr.</th>
			<th>Tytul</th>
			<th>Cena</th>
		</tr>
	</thead>');
	htp.print('<tbody>');
	FOR tmp2 IN ks LOOP
		htp.print('<tr>
			<th scope="row">'||licznik||'</th>
			<td>'||tmp2.tytul||'</td>
			<td>'||tmp2.cena||'</td>
		</tr>
		');
		licznik:=licznik+1;
	END LOOP;
	htp.print('</tbody>');
	htp.print('</table>');
	htp.print('</div>');
htp.print('</div>');
htp.bodyClose;
htp.htmlClose;

EXCEPTION
WHEN DUP_VAL_ON_INDEX THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Indeks juz wystepuje w bazie  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN INVALID_CURSOR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Kursor niepoprawny  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN TOO_MANY_ROWS THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Za duzo danych :(  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN VALUE_ERROR THEN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD: Konwersji danych  </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
WHEN OTHERS THEN
DECLARE 
    kod NUMBER:= SQLCODE;
    wiad VARCHAR2(300):= SQLERRM;
BEGIN
htp.prn('<!DOCTYPE html>
<html>
<head>
<title>Rejestracja</title>
<<link rel="stylesheet" href="http://bootswatch.com/yeti/bootstrap.min.css" >
<style>
.lol{
	border-radius:0;
}
</style>
</head>
<body>
	<nav class="navbar navbar-inverse lol">
	  <div class="container-fluid">
		<div class="navbar-header">
		 <p class="navbar-brand"> Ksiegarnia </p>
		</div>
	  </div>
	</nav>
	<div class="container">
	<div class="row">
		<div class="well">
				<h3><p class="text-danger text-center"> BLAD:  
                    '||kod||' : '||wiad||'
                 </p></h3></br>
				<p class="text-center"><a  href="panel">Powrot do panelu</a></p>
				
		</div>

	
	</div>
	</div>
</body>
</html>');
END;
END;
/

